import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { AgendarConsultaPage } from './../pages/agendar-consulta/agendar-consulta';
import { AgendarExamePage } from './../pages/agendar-exame/agendar-exame';
import { CadastroPage } from './../pages/cadastro/cadastro';
import { ContatosPage } from './../pages/contatos/contatos';
import { CadastrarConsultaPage } from '../pages/cadastrar-consulta/cadastrar-consulta';
import { CadastrarExamePage } from '../pages/cadastrar-exame/cadastrar-exame';
import { HomePage } from '../pages/home/home';
import { LoginPage } from './../pages/login/login';
import { MedicosConsultaPage } from './../pages/medicos-consulta/medicos-consulta';
import { MedicosExamesPage } from './../pages/medicos-exames/medicos-exames';
import { MyApp } from './app.component';

import { ServiceProvider } from './../providers/serviceProvider.service';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    AgendarConsultaPage,
    AgendarExamePage,
    CadastrarConsultaPage,
    CadastrarExamePage,
    CadastroPage,
    ContatosPage,
    HomePage,
    LoginPage,
    MedicosConsultaPage,
    MedicosExamesPage,
    MyApp,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
     // monthShortNames: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Dez'],
      platforms:{
        ios: {
          backButtonText: 'Voltar'
        }
      }     
    }),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    AgendarConsultaPage,
    AgendarExamePage,
    CadastrarConsultaPage,
    CadastrarExamePage,
    CadastroPage,
    ContatosPage,
    LoginPage,
    MedicosConsultaPage,
    MedicosExamesPage,
    MyApp,
    HomePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServiceProvider
  ]
})

export class AppModule {}
