import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
//Service
import { ServiceProvider } from './../../providers/serviceProvider.service';
//Pages
import { MedicosConsultaPage } from './../medicos-consulta/medicos-consulta';

@Component({
  selector: 'page-agendar-consulta',
  templateUrl: 'agendar-consulta.html',
})

export class AgendarConsultaPage {

  //Variável para guardar os dados
  espConsultas: any[];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServiceProvider,
    public loadingCtrl: LoadingController
  ) {
    this.getEspecialidades();
  }

  //Pegando os dados
  getEspecialidades() {
    let loading: Loading = this.showLoading();
    this.service.getEspecialidadesConsultas().subscribe(
      data => this.espConsultas = data,
      erro => console.log(erro)
    );
    loading.dismiss();
  }

  //Passando parâmetro do id
  medicoEscolha(codEspecialidade: any): void {
    this.navCtrl.push(MedicosConsultaPage, {
      idEspecialidade: codEspecialidade
    });
  }

  ionViewDidLoad() {}

  //Mostrando o loading
  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content: 'Por favor aguarde...'
    });  
    loading.present();
    return loading;
  }

}
