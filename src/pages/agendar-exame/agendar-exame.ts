import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading } from 'ionic-angular';

import { ServiceProvider } from './../../providers/serviceProvider.service';

import { MedicosExamesPage } from './../medicos-exames/medicos-exames';

@Component({
  selector: 'page-agendar-exame',
  templateUrl: 'agendar-exame.html',
})

export class AgendarExamePage {

  //Variável para guardar os dados
  espExames: any[];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServiceProvider,
    public loadingCtrl: LoadingController
  ){
    this.getEspecialidades();
  }

  //Pegando os dados
  getEspecialidades() {
    let loading: Loading = this.showLoading();
    this.service.getEspecialidadesExames().subscribe(
      data => this.espExames = data,
      erro => console.log(erro)
    )
    loading.dismiss();
  }

    //Passando parâmetro do id
    medicoEscolha(codEspecialidade: any): void {
      this.navCtrl.push(MedicosExamesPage, {
        idEspecialidade: codEspecialidade
      });
    }
    
  ionViewDidLoad() { }

  //Mostrando o loading
  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content: 'Por favor aguarde...'
    });  

    loading.present();

    return loading;

  }
}
