import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ServiceProvider } from '../../providers/serviceProvider.service';

@Component({
  selector: 'page-cadastrar-consulta',
  templateUrl: 'cadastrar-consulta.html',
})
export class CadastrarConsultaPage {

  convenios: any[];
  private dataConsulta = String;
  private horaConsulta = String;

  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServiceProvider
  ) {
    this.getConvenios();
   }

  //Informação e cadastro da consulta
  onConsulta(){
    const alert = this.alertCtrl.create({
      title: 'Pré-cadastro',
      subTitle: 'O pré cadastro da sua consulta foi realizado com sucesso.',
      buttons: ['Ok']
    });
    alert.present();
    this.navCtrl.setRoot(HomePage)
  }

  //Recebendo os convenios
  getConvenios() {
    this.service.getConvenios().subscribe(
      data => this.convenios = data,
      erro => console.log(erro)
    );
  }


  ionViewDidLoad() {  }



}
