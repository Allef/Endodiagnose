import { Component } from '@angular/core';
import { NavController, LoadingController, Loading } from 'ionic-angular';

import { AgendarConsultaPage } from './../agendar-consulta/agendar-consulta';
import { AgendarExamePage } from './../agendar-exame/agendar-exame';
import { LoginPage } from './../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    public navCtrl: NavController, 
    public loadingCtrl: LoadingController
  ) {

  }

  onConsulta(): void {
    this.navCtrl.push(AgendarConsultaPage)
  }

  onExame(): void {
    this.navCtrl.push(AgendarExamePage)
  }

  onLogar(): void {
    let loading: Loading = this.showLoading();
    this.navCtrl.push(LoginPage)
    loading.dismiss();
  }

  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content: 'Por favor aguarde...'
    });  

    loading.present();

    return loading;

  }
}
