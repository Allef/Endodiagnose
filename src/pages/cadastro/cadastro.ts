import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';

import { FormBuilder, Validators } from '@angular/forms';
import { ServiceProvider } from '../../providers/serviceProvider.service';

@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {

  cadastro: any = {};

  retorno:any;

  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServiceProvider,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
  ){  

    //Validação de email
    let emailRegex = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    
    //Validando formulários
    this.cadastro = this.formBuilder.group({
      nome: ['', [Validators.required, Validators.minLength(4)]],
      telefone: ['', [Validators.required, Validators.minLength(8)]],
      email: ['', Validators.compose([Validators.required, Validators.pattern(emailRegex)])],
      senha: ['', [Validators.required, Validators.minLength(6)]]
    });

  }

  //Cadastrando usuários ou não cadastrando
  postDados() {
    let retorno:any;    
    let loading: Loading = this.showLoading(); 
    this.service.postData(this.cadastro.value)
    .subscribe(
      data=> {
        console.log(data[0].mensage);
        retorno = data[0].mensage;
      },
      err=>console.log(err)
    );
    loading.dismiss();
  
    //REtornando alerta se foi ou não cadastrado
    if(retorno === 'YES'){
      
        //Alerta de não cadastro
        const alert = this.alertCtrl.create({
          title: 'Erro ao cadastrar!',
          subTitle: 'Houve um erro, tente novamente mais tarde.',
          buttons: ['Ok, Entendi']
      });

      console.log(this.cadastro.value)

      alert.present();
      
    }else{

      //Alerta de cadastro
      const alert = this.alertCtrl.create({
        title: 'Obrigado!',
        subTitle: 'Seu cadastro foi realizado com sucesso. Faça login para continuar.' ,
        buttons: ['Ok']
      });

      console.log(this.cadastro.value)

      alert.present();
      this.navCtrl.setRoot(LoginPage)
    }
  }

  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content: 'Por favor aguarde...'
    });  
    loading.present();
    return loading;
  }

}
