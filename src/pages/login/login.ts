import { Component } from '@angular/core';

import { NavController, NavParams, AlertController } from 'ionic-angular';

import { CadastroPage } from './../cadastro/cadastro';
import { HomePage } from './../home/home';
import { ServiceProvider } from '../../providers/serviceProvider.service';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loginForm : any = {};
  users:any[];

  idUsuario:number;
  nome_usuario:string;
  email_usuario:string; 

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public service : ServiceProvider,
    public alertCtrl: AlertController) {
  }  

  postLogin(req){
    this.service.postLogin(req.value)
    .subscribe(
      data => {console.log(data.mensage, data.nome, data.email);
        this.idUsuario = data.mensage;
        this.nome_usuario = data.nome;
        this.email_usuario = data.email;
        if(this.idUsuario>0){
          console.log('logado');
          this.navCtrl.setRoot(HomePage);

        }else{
          console.log('Erro logado');
          const alert = this.alertCtrl.create({
            title: 'Usuário ou senha incorretos',
            subTitle: 'Tente novamente',
            buttons: ['Ok, Entendi.']
          });
          alert.present();
        }
      },
      err => console.log(err)
    );

  }  

  ionViewDidLoad() { }

  onCreate(): void {
    this.navCtrl.push(CadastroPage);
  }

  postDados(): void {
   this.navCtrl.setRoot(HomePage)
  }

}
