import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading } from 'ionic-angular';

import { ServiceProvider } from './../../providers/serviceProvider.service';
import { CadastrarConsultaPage } from '../cadastrar-consulta/cadastrar-consulta';

@Component({
  selector: 'page-medicos-consulta',
  templateUrl: 'medicos-consulta.html',
})
export class MedicosConsultaPage {  

  medicos: any[];
  
  constructor(
    public service: ServiceProvider,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public loadingCtrl: LoadingController
  ) {
    this.getMedicos();
  }

  //Salvando o parâmetro passado numa var
  codEspecialidade: any = this.navParams.get('idEspecialidade');

  //Pegando os dados
  getMedicos() {
    let loading: Loading = this.showLoading();
    this.service.getMedicosConsultas(this.codEspecialidade).subscribe(
      data => this.medicos = data,
      erro => console.log(erro)
    );
    loading.dismiss();
  }

  concluirConsulta() {
    this.navCtrl.push(CadastrarConsultaPage)
  }

  //Testes
  ionViewDidLoad() { 
    console.log(this.codEspecialidade);
  }

  //Mostrando o loading
  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content: 'Por favor aguarde...'
    });  
    loading.present();
    return loading;
  }

}