import { Component } from '@angular/core';
import {  NavController, NavParams, AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ServiceProvider } from '../../providers/serviceProvider.service';

@Component({
  selector: 'page-cadastrar-exame',
  templateUrl: 'cadastrar-exame.html',
})
export class CadastrarExamePage {

  convenios: any[];
  private dataExame = String;
  private horaExame = String;

  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServiceProvider
  ) {
    this.getConvenios();
  }

  ionViewDidLoad() {  }

  onExame() {
    const alert = this.alertCtrl.create({
      title: 'Pré-cadastro',
      subTitle: 'O pré cadastro da sua consulta foi realizado com sucesso.',
      buttons: ['Ok']
    });
    alert.present();
    this.navCtrl.setRoot(HomePage)
  }

  //Recebendo os convenios
  getConvenios() {
    this.service.getConvenios().subscribe(
      data => this.convenios = data,
      erro => console.log(erro)
    );
  }

}
