import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading } from 'ionic-angular';

import { ServiceProvider } from './../../providers/serviceProvider.service';
import { CadastrarExamePage } from '../cadastrar-exame/cadastrar-exame';

@Component({
  selector: 'page-medicos-exames',
  templateUrl: 'medicos-exames.html',
})
export class MedicosExamesPage {

  medicos: any[];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServiceProvider,
    public loadingCtrl: LoadingController
  ) {
    this.getMedicos();
  }

  codEspecialidade: any = this.navParams.get('idEspecialidade');

  getMedicos() {
    let loading: Loading = this.showLoading();
    this.service.getMedicosExames(this.codEspecialidade).subscribe(
      data => this.medicos = data,
      erro => console.log(erro)
    );
    loading.dismiss();
  }

  concluirExame(){
    this.navCtrl.push(CadastrarExamePage)
  }

  ionViewDidLoad() { 
    console.log(this.codEspecialidade); 
  }

  //Mostrando o loading
  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content: 'Por favor aguarde...'
    });  
    loading.present();
    return loading;
  }
}
