//import { NgForm } from '@angular/forms';
import { Component } from '@angular/core';
import { NavController, NavParams, Loading, LoadingController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/serviceProvider.service';

@Component({
  selector: 'page-contatos',
  templateUrl: 'contatos.html',
})

export class ContatosPage {

  mensagem: any = {};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServiceProvider,
    public loadingCtrl: LoadingController
  ) {

  }

  postDados(req) {
    let loading: Loading = this.showLoading();
    this.service.postData(req.value)
      .subscribe(
        data=> console.log(data.mensage),
        err=>console.log(err)
      );
    loading.dismiss();
  }

  ionViewDidLoad() {
    
  }

  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content: 'Por favor aguarde...'
    });  

    loading.present();

    return loading;

  }

}
