import { Injectable } from '@angular/core';
import { Http, Headers, Response, ResponseOptions } from '@angular/http';
import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class ServiceProvider {

  api: string = 'http://localhost/API/';

  constructor(public http: Http) {}

  //recebendo especialidades e médicos da consulta
  getEspecialidadesConsultas() {
    return this.http.get(this.api+'listarEspecialidadesConsultas.php').map(res => res.json());
  }

  getMedicosConsultas(idEspecialidade: any) {
    return this.http.get(this.api+'MedicosEspecialidadeConsultas.php?codigo='+idEspecialidade).map(res => res.json());
  }

  //Recebendo especialidades dos exames
  getEspecialidadesExames() {
    return this.http.get(this.api+'listarEspecialidadesExames.php').map(res => res.json());
  }

  getMedicosExames(idEspecialidade:any) {
    return this.http.get(this.api + 'MedicosEspecialidadeExames.php?codigo='+idEspecialidade).map(res => res.json());
  }

  //Recebendo Convenios
  getConvenios() {
    return this.http.get(this.api+'convenios.php').map(res => res.json());
  }

  //Enviando dados das mensagens
  postData(parans) { 
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded'});
    return this.http.post(this.api + "cadastro.php", parans, {
      headers:headers,
      method: "POST"
    }).map(
        (res:Response)=>{return res.json()}
    );
  }

  postLogin(parans){
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    return this.http.post(this.api+"loginApp.php", parans,{
        headers:headers,
        method:"POST"
    }).map(
      (res:Response) => {return res.json()}
    );
  }
  
}