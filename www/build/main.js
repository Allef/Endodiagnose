webpackJsonp([0],{

/***/ 110:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 110;

/***/ }),

/***/ 152:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 152;

/***/ }),

/***/ 19:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ServiceProvider = (function () {
    function ServiceProvider(http) {
        this.http = http;
        this.api = 'http://localhost/API/';
    }
    //recebendo especialidades e médicos da consulta
    ServiceProvider.prototype.getEspecialidadesConsultas = function () {
        return this.http.get(this.api + 'listarEspecialidadesConsultas.php').map(function (res) { return res.json(); });
    };
    ServiceProvider.prototype.getMedicosConsultas = function (idEspecialidade) {
        return this.http.get(this.api + 'MedicosEspecialidadeConsultas.php?codigo=' + idEspecialidade).map(function (res) { return res.json(); });
    };
    //Recebendo especialidades dos exames
    ServiceProvider.prototype.getEspecialidadesExames = function () {
        return this.http.get(this.api + 'listarEspecialidadesExames.php').map(function (res) { return res.json(); });
    };
    ServiceProvider.prototype.getMedicosExames = function (idEspecialidade) {
        return this.http.get(this.api + 'MedicosEspecialidadeExames.php?codigo=' + idEspecialidade).map(function (res) { return res.json(); });
    };
    //Recebendo Convenios
    ServiceProvider.prototype.getConvenios = function () {
        return this.http.get(this.api + 'convenios.php').map(function (res) { return res.json(); });
    };
    //Enviando dados das mensagens
    ServiceProvider.prototype.postData = function (parans) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this.http.post(this.api + "cadastro.php", parans, {
            headers: headers,
            method: "POST"
        }).map(function (res) { return res.json(); });
    };
    ServiceProvider.prototype.postLogin = function (parans) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this.http.post(this.api + "loginApp.php", parans, {
            headers: headers,
            method: "POST"
        }).map(function (res) { return res.json(); });
    };
    return ServiceProvider;
}());
ServiceProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceProvider);

//# sourceMappingURL=serviceProvider.service.js.map

/***/ }),

/***/ 192:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AgendarConsultaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_serviceProvider_service__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__medicos_consulta_medicos_consulta__ = __webpack_require__(194);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//Service

//Pages

var AgendarConsultaPage = (function () {
    function AgendarConsultaPage(navCtrl, navParams, service, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.getEspecialidades();
    }
    //Pegando os dados
    AgendarConsultaPage.prototype.getEspecialidades = function () {
        var _this = this;
        var loading = this.showLoading();
        this.service.getEspecialidadesConsultas().subscribe(function (data) { return _this.espConsultas = data; }, function (erro) { return console.log(erro); });
        loading.dismiss();
    };
    //Passando parâmetro do id
    AgendarConsultaPage.prototype.medicoEscolha = function (codEspecialidade) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__medicos_consulta_medicos_consulta__["a" /* MedicosConsultaPage */], {
            idEspecialidade: codEspecialidade
        });
    };
    AgendarConsultaPage.prototype.ionViewDidLoad = function () { };
    //Mostrando o loading
    AgendarConsultaPage.prototype.showLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Por favor aguarde...'
        });
        loading.present();
        return loading;
    };
    return AgendarConsultaPage;
}());
AgendarConsultaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-agendar-consulta',template:/*ion-inline-start:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\agendar-consulta\agendar-consulta.html"*/'<ion-header>\n\n  <ion-navbar color=\'secondary\'>\n\n    <ion-title>ESPECIALIDADES</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-list no-lines>\n\n    <!-- Recebendo as informações e mostrando as consultas -->\n\n    <button ion-item *ngFor="let espConsulta of espConsultas" (click)="medicoEscolha(espConsulta.id_especialidades)">\n\n      <h3>{{ espConsulta.titulo }}</h3>\n\n    </button>\n\n  </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\agendar-consulta\agendar-consulta.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_serviceProvider_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
], AgendarConsultaPage);

//# sourceMappingURL=agendar-consulta.js.map

/***/ }),

/***/ 194:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MedicosConsultaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_serviceProvider_service__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cadastrar_consulta_cadastrar_consulta__ = __webpack_require__(195);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MedicosConsultaPage = (function () {
    function MedicosConsultaPage(service, navCtrl, navParams, loadingCtrl) {
        this.service = service;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        //Salvando o parâmetro passado numa var
        this.codEspecialidade = this.navParams.get('idEspecialidade');
        this.getMedicos();
    }
    //Pegando os dados
    MedicosConsultaPage.prototype.getMedicos = function () {
        var _this = this;
        var loading = this.showLoading();
        this.service.getMedicosConsultas(this.codEspecialidade).subscribe(function (data) { return _this.medicos = data; }, function (erro) { return console.log(erro); });
        loading.dismiss();
    };
    MedicosConsultaPage.prototype.concluirConsulta = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__cadastrar_consulta_cadastrar_consulta__["a" /* CadastrarConsultaPage */]);
    };
    //Testes
    MedicosConsultaPage.prototype.ionViewDidLoad = function () {
        console.log(this.codEspecialidade);
    };
    //Mostrando o loading
    MedicosConsultaPage.prototype.showLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Por favor aguarde...'
        });
        loading.present();
        return loading;
    };
    return MedicosConsultaPage;
}());
MedicosConsultaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-medicos-consulta',template:/*ion-inline-start:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\medicos-consulta\medicos-consulta.html"*/'<ion-header>\n\n  <ion-navbar color=\'secondary\'>\n\n    <ion-title>MÉDICOS DA ESPECIALIDADE</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n  <ion-list>\n\n    <ion-item no-lines>\n\n      <!-- Recebendo as informações e mostrando as consultas -->\n\n      <button ion-item *ngFor="let medico of medicos" (click)="concluirConsulta()">\n\n        <h3>{{ medico.nome }}</h3>\n\n      </button>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\medicos-consulta\medicos-consulta.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_serviceProvider_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
], MedicosConsultaPage);

//# sourceMappingURL=medicos-consulta.js.map

/***/ }),

/***/ 195:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CadastrarConsultaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_serviceProvider_service__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CadastrarConsultaPage = (function () {
    function CadastrarConsultaPage(alertCtrl, navCtrl, navParams, service) {
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.dataConsulta = String;
        this.horaConsulta = String;
        this.getConvenios();
    }
    //Informação e cadastro da consulta
    CadastrarConsultaPage.prototype.onConsulta = function () {
        var alert = this.alertCtrl.create({
            title: 'Pré-cadastro',
            subTitle: 'O pré cadastro da sua consulta foi realizado com sucesso.',
            buttons: ['Ok']
        });
        alert.present();
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    //Recebendo os convenios
    CadastrarConsultaPage.prototype.getConvenios = function () {
        var _this = this;
        this.service.getConvenios().subscribe(function (data) { return _this.convenios = data; }, function (erro) { return console.log(erro); });
    };
    CadastrarConsultaPage.prototype.ionViewDidLoad = function () { };
    return CadastrarConsultaPage;
}());
CadastrarConsultaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-cadastrar-consulta',template:/*ion-inline-start:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\cadastrar-consulta\cadastrar-consulta.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>CADASTRAR CONSULTA</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-list no-lines>\n\n    <ion-item>\n\n      <ion-label>Convenio</ion-label>\n\n        <ion-select [(ngModel)]="convenio">\n\n          <ion-option *ngFor="let convenio of convenios" [value]="convenio" >{{ convenio.nome }}</ion-option>\n\n      </ion-select>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list>\n\n    <ion-item>\n\n      <ion-label>Selecione a data:</ion-label>\n\n      <ion-datetime displayFormat="DD: MM YYYY" pickerFormat="DD MM YYYY" max="2020-12-31" [(ngModel)]="dataConsulta"></ion-datetime>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>Selecione a hora:</ion-label>\n\n      <ion-datetime displayFormat="HH: mm" pickerFormat="HH mm" [(ngModel)]="horaConsulta"></ion-datetime>\n\n    </ion-item>\n\n      <br/>\n\n      <button ion-button block color="secondary" (click)="onConsulta()">Enviar</button>\n\n  </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\cadastrar-consulta\cadastrar-consulta.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__providers_serviceProvider_service__["a" /* ServiceProvider */]])
], CadastrarConsultaPage);

//# sourceMappingURL=cadastrar-consulta.js.map

/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AgendarExamePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_serviceProvider_service__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__medicos_exames_medicos_exames__ = __webpack_require__(197);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AgendarExamePage = (function () {
    function AgendarExamePage(navCtrl, navParams, service, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.getEspecialidades();
    }
    //Pegando os dados
    AgendarExamePage.prototype.getEspecialidades = function () {
        var _this = this;
        var loading = this.showLoading();
        this.service.getEspecialidadesExames().subscribe(function (data) { return _this.espExames = data; }, function (erro) { return console.log(erro); });
        loading.dismiss();
    };
    //Passando parâmetro do id
    AgendarExamePage.prototype.medicoEscolha = function (codEspecialidade) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__medicos_exames_medicos_exames__["a" /* MedicosExamesPage */], {
            idEspecialidade: codEspecialidade
        });
    };
    AgendarExamePage.prototype.ionViewDidLoad = function () { };
    //Mostrando o loading
    AgendarExamePage.prototype.showLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Por favor aguarde...'
        });
        loading.present();
        return loading;
    };
    return AgendarExamePage;
}());
AgendarExamePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-agendar-exame',template:/*ion-inline-start:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\agendar-exame\agendar-exame.html"*/'<ion-header>\n\n  <ion-navbar color=\'secondary\'>\n\n    <ion-title>ESPECIALIDADES</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-list no-lines>\n\n    <!-- Recebendo as informações e mostrando os Exames -->\n\n    <button ion-item *ngFor="let espExame of espExames" (click)="medicoEscolha(espExame.id_esp_exame)">\n\n      <h3>{{ espExame.titulo }}</h3>\n\n    </button>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\agendar-exame\agendar-exame.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_serviceProvider_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
], AgendarExamePage);

//# sourceMappingURL=agendar-exame.js.map

/***/ }),

/***/ 197:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MedicosExamesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_serviceProvider_service__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cadastrar_exame_cadastrar_exame__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MedicosExamesPage = (function () {
    function MedicosExamesPage(navCtrl, navParams, service, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.codEspecialidade = this.navParams.get('idEspecialidade');
        this.getMedicos();
    }
    MedicosExamesPage.prototype.getMedicos = function () {
        var _this = this;
        var loading = this.showLoading();
        this.service.getMedicosExames(this.codEspecialidade).subscribe(function (data) { return _this.medicos = data; }, function (erro) { return console.log(erro); });
        loading.dismiss();
    };
    MedicosExamesPage.prototype.concluirExame = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__cadastrar_exame_cadastrar_exame__["a" /* CadastrarExamePage */]);
    };
    MedicosExamesPage.prototype.ionViewDidLoad = function () {
        console.log(this.codEspecialidade);
    };
    //Mostrando o loading
    MedicosExamesPage.prototype.showLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Por favor aguarde...'
        });
        loading.present();
        return loading;
    };
    return MedicosExamesPage;
}());
MedicosExamesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-medicos-exames',template:/*ion-inline-start:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\medicos-exames\medicos-exames.html"*/'<ion-header>\n\n\n\n  <ion-navbar color=\'secondary\'>\n\n    <ion-title>MÉDICOS DA ESPECIALIDADE</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  \n\n  <ion-list>\n\n    <ion-item no-lines>\n\n      <!-- Recebendo as informações e mostrando as consultas -->\n\n      <button ion-item *ngFor="let medico of medicos" (click)="concluirExame()">\n\n        <h3>{{ medico.nome }}</h3>\n\n      </button>\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\medicos-exames\medicos-exames.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_serviceProvider_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
], MedicosExamesPage);

//# sourceMappingURL=medicos-exames.js.map

/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CadastrarExamePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_serviceProvider_service__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CadastrarExamePage = (function () {
    function CadastrarExamePage(alertCtrl, navCtrl, navParams, service) {
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.dataExame = String;
        this.horaExame = String;
        this.getConvenios();
    }
    CadastrarExamePage.prototype.ionViewDidLoad = function () { };
    CadastrarExamePage.prototype.onExame = function () {
        var alert = this.alertCtrl.create({
            title: 'Pré-cadastro',
            subTitle: 'O pré cadastro da sua consulta foi realizado com sucesso.',
            buttons: ['Ok']
        });
        alert.present();
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    //Recebendo os convenios
    CadastrarExamePage.prototype.getConvenios = function () {
        var _this = this;
        this.service.getConvenios().subscribe(function (data) { return _this.convenios = data; }, function (erro) { return console.log(erro); });
    };
    return CadastrarExamePage;
}());
CadastrarExamePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-cadastrar-exame',template:/*ion-inline-start:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\cadastrar-exame\cadastrar-exame.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>CADASTRAR EXAME</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-list no-lines>\n\n    <ion-item>\n\n      <ion-label>Convenio</ion-label>\n\n        <ion-select [(ngModel)]="convenio">\n\n          <ion-option *ngFor="let convenio of convenios" [value]="convenio">{{ convenio.nome }}</ion-option>\n\n      </ion-select>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list>\n\n    <ion-item>\n\n      <ion-label>Selecione a data:</ion-label>\n\n      <ion-datetime displayFormat="DD: MM YYYY" pickerFormat="DD MM YYYY" max="2020-12-31" [(ngModel)]="dataExame"></ion-datetime>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>Selecione a hora:</ion-label>\n\n      <ion-datetime displayFormat="HH: mm" pickerFormat="HH mm" [(ngModel)]="horaExame"></ion-datetime>\n\n    </ion-item>\n\n      <button ion-button block color="secondary" (click)="onExame()">Enviar</button>\n\n  </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\cadastrar-exame\cadastrar-exame.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__providers_serviceProvider_service__["a" /* ServiceProvider */]])
], CadastrarExamePage);

//# sourceMappingURL=cadastrar-exame.js.map

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CadastroPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__login_login__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_serviceProvider_service__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CadastroPage = (function () {
    function CadastroPage(alertCtrl, navCtrl, navParams, service, formBuilder, loadingCtrl) {
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.formBuilder = formBuilder;
        this.loadingCtrl = loadingCtrl;
        this.cadastro = {};
        //Validação de email
        var emailRegex = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
        //Validando formulários
        this.cadastro = this.formBuilder.group({
            nome: ['', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].minLength(4)]],
            telefone: ['', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].minLength(8)]],
            email: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].pattern(emailRegex)])],
            senha: ['', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].minLength(6)]]
        });
    }
    //Cadastrando usuários ou não cadastrando
    CadastroPage.prototype.postDados = function () {
        var retorno;
        var loading = this.showLoading();
        this.service.postData(this.cadastro.value)
            .subscribe(function (data) {
            console.log(data[0].mensage);
            retorno = data[0].mensage;
        }, function (err) { return console.log(err); });
        loading.dismiss();
        //REtornando alerta se foi ou não cadastrado
        if (retorno === 'YES') {
            //Alerta de não cadastro
            var alert_1 = this.alertCtrl.create({
                title: 'Erro ao cadastrar!',
                subTitle: 'Houve um erro, tente novamente mais tarde.',
                buttons: ['Ok, Entendi']
            });
            console.log(this.cadastro.value);
            alert_1.present();
        }
        else {
            //Alerta de cadastro
            var alert_2 = this.alertCtrl.create({
                title: 'Obrigado!',
                subTitle: 'Seu cadastro foi realizado com sucesso. Faça login para continuar.',
                buttons: ['Ok']
            });
            console.log(this.cadastro.value);
            alert_2.present();
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__login_login__["a" /* LoginPage */]);
        }
    };
    CadastroPage.prototype.showLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Por favor aguarde...'
        });
        loading.present();
        return loading;
    };
    return CadastroPage;
}());
CadastroPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
        selector: 'page-cadastro',template:/*ion-inline-start:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\cadastro\cadastro.html"*/'<ion-header>\n\n  <ion-navbar color=\'secondary\'>\n\n    <ion-title>CADASTRO</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n    <!-- Item principal -->\n\n    <h1 text-center>\n\n      <ion-icon class="auth-icon" name="ios-person-add" color="secondary"></ion-icon>\n\n    </h1>\n\n    \n\n    <!-- Declaração form e suas funções -->\n\n    <form [formGroup]="cadastro" (ngSubmit)="postDados()">\n\n      <!-- Itens para cadastro -->\n\n        <ion-item>\n\n          <ion-label floating>Nome:</ion-label>\n\n          <ion-input text-center type="text" placeholder="Meu Nome" formControlName="nome" ></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label floating>Telefone:</ion-label>\n\n          <ion-input text-center type="number" placeholder="987654321" formControlName="telefone" ></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label floating>Email:</ion-label>\n\n          <ion-input text-center type="email" placeholder="meu@email.com" formControlName="email"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label floating>Senha:</ion-label>\n\n          <ion-input text-center type="password" placeholder="xxxxxx" formControlName="senha"></ion-input>\n\n        </ion-item>\n\n        \n\n        <!-- Botão para enviar informações -->\n\n        <button color="secondary" [disabled]="cadastro.invalid" ion-button full block>Criar conta</button>\n\n    </form>\n\n  \n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\cadastro\cadastro.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__providers_serviceProvider_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* LoadingController */]])
], CadastroPage);

//# sourceMappingURL=cadastro.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContatosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_serviceProvider_service__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//import { NgForm } from '@angular/forms';



var ContatosPage = (function () {
    function ContatosPage(navCtrl, navParams, service, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.mensagem = {};
    }
    ContatosPage.prototype.postDados = function (req) {
        var loading = this.showLoading();
        this.service.postData(req.value)
            .subscribe(function (data) { return console.log(data.mensage); }, function (err) { return console.log(err); });
        loading.dismiss();
    };
    ContatosPage.prototype.ionViewDidLoad = function () {
    };
    ContatosPage.prototype.showLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Por favor aguarde...'
        });
        loading.present();
        return loading;
    };
    return ContatosPage;
}());
ContatosPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-contatos',template:/*ion-inline-start:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\contatos\contatos.html"*/'<ion-header>\n\n  <ion-navbar color=\'secondary\'>\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu" color="light"></ion-icon>\n\n      </button>\n\n    <ion-title text-center>CONTATOS</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <form #f="ngForm" (ngSubmit)="postDados(f)">\n\n    <ion-list>\n\n        <ion-item>\n\n          <ion-label floating>Nome:</ion-label>\n\n          <ion-input type="text" name="nome" ngModel></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label floating>Email:</ion-label>\n\n          <ion-input type="text" name="email" ngModel></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label floating>Título:</ion-label>\n\n          <ion-input type="text" name="titulo" ngModel ></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label floating>Mesagem:</ion-label>\n\n          <ion-input type="text" name="mensagem" ngModel></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n          <button ion-button full block color="secondary">Enviar</button>\n\n        </ion-item>\n\n    </ion-list>\n\n  </form>\n\n\n\n  <!-- <h4 text-center>Fale conosco</h4>\n\n\n\n  <form [formGroup]="mensagem" (ngSubmit)="postDados()">\n\n    <ion-item>\n\n      <ion-label>Nome:</ion-label>\n\n      <ion-input type="text"  name="nome" formControlName="nome"></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label>Email:</ion-label>\n\n      <ion-input type="email"  name="email" formControlName="email"></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label>Título:</ion-label>\n\n      <ion-input type="text"  name="titulo" formControlName="titulo"></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label>Mensagem:</ion-label>\n\n      <ion-input type="text" name="mensagem" formControlName="mensagem"></ion-input>\n\n    </ion-item>\n\n    <br/>\n\n\n\n    <button [disabled]="mensagem.invalid" ion-button full block color="secondary">Enviar</button>\n\n  </form> -->\n\n\n\n  <ion-list no-lines>\n\n    <ion-item>\n\n      <ion-icon name="person" color="secondary" item-left></ion-icon>\n\n      <p>Clínica Endodiagnose</p>\n\n    </ion-item>\n\n    <ion-item>\n\n        <ion-icon name="call" color="secondary" item-left></ion-icon>\n\n      <p>(85) 3087 1112</p>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-icon name="phone-portrait" color="secondary" item-left></ion-icon>\n\n      <p>(85) 9 8901 1521 | 9 9788 0912</p>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-icon name="mail" color="secondary" item-left></ion-icon>\n\n      <p>contato@endodiagnose.com.br</p>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-icon name="pin" color="secondary" item-left></ion-icon>\n\n        <p>Av. Santos Dumont, 1510, \n\n        <br/>Edifício Manhattan Square Garden <wbr>(Sala 407) \n\n        <br/>4º andar - Aldeota,\n\n        <br/> Fortaleza-CE CEP: 60.150-161</p>\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\contatos\contatos.html"*/,
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__providers_serviceProvider_service__["a" /* ServiceProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_serviceProvider_service__["a" /* ServiceProvider */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]) === "function" && _d || Object])
], ContatosPage);

var _a, _b, _c, _d;
//# sourceMappingURL=contatos.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(224);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 224:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_agendar_consulta_agendar_consulta__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_agendar_exame_agendar_exame__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_cadastro_cadastro__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_contatos_contatos__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_cadastrar_consulta_cadastrar_consulta__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_cadastrar_exame_cadastrar_exame__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_home_home__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_login_login__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_medicos_consulta_medicos_consulta__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_medicos_exames_medicos_exames__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__app_component__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_serviceProvider_service__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_status_bar__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_splash_screen__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__angular_http__ = __webpack_require__(193);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__pages_agendar_consulta_agendar_consulta__["a" /* AgendarConsultaPage */],
            __WEBPACK_IMPORTED_MODULE_4__pages_agendar_exame_agendar_exame__["a" /* AgendarExamePage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_cadastrar_consulta_cadastrar_consulta__["a" /* CadastrarConsultaPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_cadastrar_exame_cadastrar_exame__["a" /* CadastrarExamePage */],
            __WEBPACK_IMPORTED_MODULE_5__pages_cadastro_cadastro__["a" /* CadastroPage */],
            __WEBPACK_IMPORTED_MODULE_6__pages_contatos_contatos__["a" /* ContatosPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_medicos_consulta_medicos_consulta__["a" /* MedicosConsultaPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_medicos_exames_medicos_exames__["a" /* MedicosExamesPage */],
            __WEBPACK_IMPORTED_MODULE_13__app_component__["a" /* MyApp */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_13__app_component__["a" /* MyApp */], {
                // monthShortNames: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Dez'],
                platforms: {
                    ios: {
                        backButtonText: 'Voltar'
                    }
                }
            }, {
                links: []
            }),
            __WEBPACK_IMPORTED_MODULE_17__angular_http__["c" /* HttpModule */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_3__pages_agendar_consulta_agendar_consulta__["a" /* AgendarConsultaPage */],
            __WEBPACK_IMPORTED_MODULE_4__pages_agendar_exame_agendar_exame__["a" /* AgendarExamePage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_cadastrar_consulta_cadastrar_consulta__["a" /* CadastrarConsultaPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_cadastrar_exame_cadastrar_exame__["a" /* CadastrarExamePage */],
            __WEBPACK_IMPORTED_MODULE_5__pages_cadastro_cadastro__["a" /* CadastroPage */],
            __WEBPACK_IMPORTED_MODULE_6__pages_contatos_contatos__["a" /* ContatosPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_medicos_consulta_medicos_consulta__["a" /* MedicosConsultaPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_medicos_exames_medicos_exames__["a" /* MedicosExamesPage */],
            __WEBPACK_IMPORTED_MODULE_13__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_15__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_16__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_14__providers_serviceProvider_service__["a" /* ServiceProvider */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 269:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_contatos_contatos__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */];
        this.initializeApp();
        //Usando para chamar páginas na lista
        this.pages = [
            { title: 'Início', component: __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */] },
            { title: 'Contatos', component: __WEBPACK_IMPORTED_MODULE_4__pages_contatos_contatos__["a" /* ContatosPage */] }
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        this.nav.setRoot(page.component);
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\app\app.html"*/'<ion-menu [content]="content">\n\n  <ion-header>\n\n    <ion-toolbar  color=\'secondary\'>\n\n      <ion-title>Menu</ion-title>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n\n\n  <ion-content>\n\n    <ion-list>\n\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n\n        {{p.title}}\n\n      </button>\n\n    </ion-list>\n\n  </ion-content>\n\n\n\n</ion-menu>\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\app\app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 41:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__agendar_consulta_agendar_consulta__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__agendar_exame_agendar_exame__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePage = (function () {
    function HomePage(navCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
    }
    HomePage.prototype.onConsulta = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__agendar_consulta_agendar_consulta__["a" /* AgendarConsultaPage */]);
    };
    HomePage.prototype.onExame = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__agendar_exame_agendar_exame__["a" /* AgendarExamePage */]);
    };
    HomePage.prototype.onLogar = function () {
        var loading = this.showLoading();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
        loading.dismiss();
    };
    HomePage.prototype.showLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Por favor aguarde...'
        });
        loading.present();
        return loading;
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\home\home.html"*/'<ion-header>\n\n  <ion-navbar color=\'secondary\'>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu" color="light"></ion-icon>\n\n    </button>\n\n    <ion-title text-center>ENDODIAGNOSE</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n \n\n  <h1 text-center>\n\n    <ion-img width="100" height="100" src="../../assets/icon/favicon.png"></ion-img>\n\n  </h1>\n\n\n\n  <div text-center>\n\n    <h2 color="secondary">ENDODIAGNOSE</h2>\n\n    <p color="secondary">GASTROENDOSCOPIA E COLOPROCTOLOGIA</p>\n\n  </div>\n\n  \n\n  <br/>\n\n\n\n  <div>\n\n    <button ion-button color="secondary" full (click)="onConsulta()">AGENDAR CONSULTA</button>\n\n    <button ion-button color="secondary" full (click)="onExame()">AGENDAR EXAME</button>\n\n  </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\home\home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cadastro_cadastro__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_serviceProvider_service__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, service, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.loginForm = {};
    }
    LoginPage.prototype.postLogin = function (req) {
        var _this = this;
        this.service.postLogin(req.value)
            .subscribe(function (data) {
            console.log(data.mensage, data.nome, data.email);
            _this.idUsuario = data.mensage;
            _this.nome_usuario = data.nome;
            _this.email_usuario = data.email;
            if (_this.idUsuario > 0) {
                console.log('logado');
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
            }
            else {
                console.log('Erro logado');
                var alert = _this.alertCtrl.create({
                    title: 'Usuário ou senha incorretos',
                    subTitle: 'Tente novamente',
                    buttons: ['Ok, Entendi.']
                });
                alert.present();
            }
        }, function (err) { return console.log(err); });
    };
    LoginPage.prototype.ionViewDidLoad = function () { };
    LoginPage.prototype.onCreate = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__cadastro_cadastro__["a" /* CadastroPage */]);
    };
    LoginPage.prototype.postDados = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\login\login.html"*/'<ion-header>\n\n\n\n  <ion-navbar color=\'secondary\'>\n\n    <ion-title text-center>ENTRAR</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <h1 text-center>\n\n      <ion-icon class="auth-icon" name="ios-person" color="secondary"></ion-icon>\n\n    </h1>\n\n    \n\n    <!-- Declaração form e suas funções\n\n    <form [formGroup]="login" (ngSubmit)="postDados()">\n\n      Itens para logar\n\n      <ion-item>\n\n        <ion-label floating>Nome:</ion-label>\n\n        <ion-input type="email" placeholder="Email" value="email@email.com" formControlName="email"></ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n          <ion-label floating>Senha:</ion-label>\n\n        <ion-input type="password" placeholder="Senha" value="123456" formControlName="senha"></ion-input>\n\n      </ion-item>\n\n\n\n      <br/>\n\n     Botão para enviar informações \n\n      <button color="secondary" ion-button full block [disabled]="login.invalid">Entrar</button>\n\n    </form>-->\n\n\n\n    <form #formCadastro="ngForm" (ngSubmit)="postLogin(formCadastro)">\n\n        <ion-list>  \n\n          <ion-item>\n\n              <ion-label floating>Email</ion-label>\n\n              <ion-input type="text" name="email" ngModel #email="ngModel"></ion-input>\n\n          </ion-item>\n\n           \n\n          <ion-item>\n\n            <ion-label floating>Senha</ion-label>\n\n            <ion-input type="password" name="senha" ngModel #senha="ngModel"></ion-input>\n\n          </ion-item>\n\n          <br/>\n\n          <button [disabled]="loginForm.invalid" ion-button full block color="secondary">Enviar</button>    \n\n        </ion-list>\n\n      </form>\n\n      <button color="secondary" ion-button full block (click)="onCreate()">Criar conta</button>\n\n  \n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\a113f\OneDrive\Documents\Endodiagnose\src\pages\login\login.html"*/,
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__providers_serviceProvider_service__["a" /* ServiceProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__providers_serviceProvider_service__["a" /* ServiceProvider */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _d || Object])
], LoginPage);

var _a, _b, _c, _d;
//# sourceMappingURL=login.js.map

/***/ })

},[205]);
//# sourceMappingURL=main.js.map