<?php
include("conexao.php");
header("Content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");

//RECUPERAÇÃO DO FORMULÁRIO
    $data = file_get_contents("php://input");
    $objData = json_decode($data);

    // TRANSFORMA OS DADOS
    $nome = $objData->nome;    
    $telefone = $objData->telefone;
    $email = $objData->email;
    $senha = $objData->senha; 
    $convenio = $objData->convenio;
    $data_exame = $objData->data_do_exame;
    $especialidade = $objData->especialidade;
  

//echo $nome."\n".$email."\n".$telefone."\n".$convenio."\n".$data_consulta."\n".$especialidade;

$sql = "INSERT INTO agenda_exames
(nome, email, telefone, convenio, data_do_exame, especialidade)
VALUES
(?,?,?,?,?,?)";

  $stm = $conn->prepare($sql);
  mysqli_set_charset($conn,"utf8");
  $stm->bind_param("ssssss", $nome,$email,$telefone,$convenio,$data_exame,$especialidade);

  if($stm->execute()){
    $retorno = array("menssage" => "YES");
  }else{
    $retorno = array("menssage" => "NO");
  }

  echo json_encode($retorno);

  //$stm->execute();
  $stm->close();
  $conn->close();

?>
