-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 20-Out-2017 às 01:58
-- Versão do servidor: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `endodiagnose_app`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `agenda_consulta`
--

CREATE TABLE `agenda_consulta` (
  `id_agenda_consulta` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `telefone` varchar(15) DEFAULT NULL,
  `convenio` varchar(45) DEFAULT NULL,
  `data_da_consulta` varchar(20) DEFAULT NULL,
  `especialidade` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `agenda_consulta`
--

INSERT INTO `agenda_consulta` (`id_agenda_consulta`, `nome`, `email`, `telefone`, `convenio`, `data_da_consulta`, `especialidade`) VALUES
(1, 'ISRAEL HUDSON ARAGAO BARBOSA', 'israelhudson@gmail.com', '85985638045', 'Hapvida', '2017-09-21', 'NÃ£o inserido'),
(2, 'ISRAEL HUDSON ARAGAO BARBOSA', 'israelhudson@gmail.com', '85985638045', 'Hapvida', '2017-09-21', 'NÃ£o inserido'),
(3, 'ISRAEL HUDSON ARAGAO BARBOSA', 'israelhudson@gmail.com', '85985638045', 'Hapvida', '2017-09-21', 'NÃ£o inserido'),
(4, 'ISRAEL HUDSON ARAGAO BARBOSA', 'israelhudson@gmail.com', '85985638045', 'Hapvida', '2017-09-21', 'Não inserido'),
(5, 'ISRAEL HUDSON ARAGAO BARBOSA', 'israelhudson@gmail.com', '85985638045', 'Hapvida', '2017-09-21', 'Não inserido'),
(6, 'Não inserido', 'Não inserido', 'Não inserido', 'Não inserido', 'Não inserido', 'Não inserido'),
(7, 'Israel', 'asdas@d', '444', 'Quarto', '4444', 'COLOPROCTOLOGIA'),
(8, 'Habaha', 'vVVa', '979797', 'Unimed', '676797', 'NUTROLOGIA'),
(9, 'Jshha', 'bsvga', '6494', 'Issec', '386/588', 'GASTROENTEROLOGIA');

-- --------------------------------------------------------

--
-- Estrutura da tabela `agenda_exame`
--

CREATE TABLE `agenda_exame` (
  `id_agenda_exame` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `telefone` varchar(15) DEFAULT NULL,
  `convenio` varchar(45) DEFAULT NULL,
  `data_da_consulta` varchar(20) DEFAULT NULL,
  `especialidade` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `agenda_exame`
--

INSERT INTO `agenda_exame` (`id_agenda_exame`, `nome`, `email`, `telefone`, `convenio`, `data_da_consulta`, `especialidade`) VALUES
(6, 'Não inserido', 'Não inserido', 'Não inserido', 'Não inserido', 'Não inserido', 'Não inserido'),
(7, 'ISRAEL HUDSON ARAGAO BARBOSA', 'israelhudson@gmail.com', '85985638045', 'Camed', '2017-09-12', 'Não inserido');

-- --------------------------------------------------------

--
-- Estrutura da tabela `especialidades`
--

CREATE TABLE `especialidades` (
  `id_especialidades` int(11) NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `descricao` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `especialidades`
--

INSERT INTO `especialidades` (`id_especialidades`, `titulo`, `descricao`) VALUES
(1, 'COLOPROCTOLOGIA', 'Especialidade médica que estuda as doenças do intestino grosso, também conhecido como'),
(2, 'NUTROLOGIA', 'Especialidade médica que estuda, pesquisa e avalia os benefícios e malefícios causados pela'),
(3, 'GASTROENTEROLOGIA', 'Especialidade médica que cuida do Sistema Digestivo. O especialista em Gastroenterologia'),
(4, 'CIRURGIA DE CABEÇA E PESCOÇO', 'Especialidade cirúrgica que trata principalmente dos tumores benignos e malignos da região');

-- --------------------------------------------------------

--
-- Estrutura da tabela `esp_exame`
--

CREATE TABLE `esp_exame` (
  `id_esp_exame` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `esp_exame`
--

INSERT INTO `esp_exame` (`id_esp_exame`, `titulo`) VALUES
(1, 'Endoscopia Digestiva Alta | Gastroenterologia'),
(2, 'Ecoendoscopia'),
(3, 'Colonoscopia'),
(4, 'Colangiografia Endoscópica Retrógrada – CPRE'),
(5, 'Retossigmoidoscopia'),
(6, 'Balão Intragástrico'),
(7, 'Teste'),
(8, 'Manometria Anorretal e Esofágica'),
(9, 'pHmetria'),
(10, 'Medicina Preventiva');

-- --------------------------------------------------------

--
-- Estrutura da tabela `medicos`
--

CREATE TABLE `medicos` (
  `id_medicos` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `crm` int(10) NOT NULL,
  `rqe` int(10) NOT NULL,
  `especialidade_id` int(11) NOT NULL,
  `esp_exame_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `medicos`
--

INSERT INTO `medicos` (`id_medicos`, `nome`, `crm`, `rqe`, `especialidade_id`, `esp_exame_id`) VALUES
(1, 'Dr. Francisco Oliveira', 7368, 4557, 3, 1),
(2, 'Dr. Gabriel Soares', 8284, 3763, 1, 3),
(3, 'Dra. Nadia Elis Costa', 15, 0, 2, 10),
(4, 'Dr. Roger Ximenes do Prado', 7734, 6267, 4, 0),
(5, 'Dr. Themistocles Carvalho', 10791, 0, 3, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agenda_consulta`
--
ALTER TABLE `agenda_consulta`
  ADD PRIMARY KEY (`id_agenda_consulta`);

--
-- Indexes for table `agenda_exame`
--
ALTER TABLE `agenda_exame`
  ADD PRIMARY KEY (`id_agenda_exame`);

--
-- Indexes for table `especialidades`
--
ALTER TABLE `especialidades`
  ADD PRIMARY KEY (`id_especialidades`);

--
-- Indexes for table `esp_exame`
--
ALTER TABLE `esp_exame`
  ADD PRIMARY KEY (`id_esp_exame`);

--
-- Indexes for table `medicos`
--
ALTER TABLE `medicos`
  ADD PRIMARY KEY (`id_medicos`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agenda_consulta`
--
ALTER TABLE `agenda_consulta`
  MODIFY `id_agenda_consulta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `agenda_exame`
--
ALTER TABLE `agenda_exame`
  MODIFY `id_agenda_exame` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `especialidades`
--
ALTER TABLE `especialidades`
  MODIFY `id_especialidades` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `esp_exame`
--
ALTER TABLE `esp_exame`
  MODIFY `id_esp_exame` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `medicos`
--
ALTER TABLE `medicos`
  MODIFY `id_medicos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
